#define RELAY 3

int impulsion_timimg = 0;

void setup() {
  pinMode(RELAY, OUTPUT);
  Serial.begin(112500);
  Serial.println("starting program");
}

void up() {
  Serial.println("start computer");
  digitalWrite(RELAY, HIGH);
  delay(impulsion_timimg);
  digitalWrite(RELAY, LOW);

  impulsion_timimg = 0;
  Serial.println("stop");
}

void loop() {
  if (impulsion_timimg>0) {
    up();
  }

  delay(1);
}

void serialEvent() {
  while(Serial.available()) {
    char inChar = (char)Serial.read();

    // Short impulsion
    if (inChar == 's') {
       impulsion_timimg = 500; // ms
    }
    // Long impulsion
    else if (inChar == 'l') {
       impulsion_timimg = 5000; // ms
    }
  }
}

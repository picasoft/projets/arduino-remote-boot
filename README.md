# Arduino Remote Reboot

Solution un peu sale pour redémarrer Alice depuis Caribou lorsqu'elle est cassée.

Le principe est simplement de brancher un arduino en USB à Caribou d'un côté et sur les headers de la carte mère d'Alice de l'autre.
Par un envoi de certains caractères à l'arduino, le micro-controlleur envoie un signal de reboot à Alice.

## Utilisation

Pour envoyer les caractères à l'arduino, il suffit d'appeler
```bash
./remote-reboot.sh
# ou ./remote-reboot.sh -l pour un appui long
```

## Mise en place

Pour mettre en place ce système :

1. Brancher l'arduino en USB
2. Ouvrir et téléverser `main.ino` via l'IDE arduino
3. Exécuter `sudo ln -s `realpath 10-alice-rebooter.rules` /etc/udev/rules.d && sudo udevadm control --reload-rules`
4. Refaire les connexions du schéma suivant sur l'arduino

![sketch](sketch.jpg)

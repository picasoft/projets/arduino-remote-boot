#! /usr/bin/env bash

char="s" # Short impulse (0.5s) by default

# Parse arguments
while [[ $# -gt 0 ]]; do
    case $1 in
        -l|--long)
            char="l" # Long impulse (5 sec) when required
            shift # pass flag
            ;;
        *)
            echo "Unknown option $1"
            exit 1
            ;;
    esac
done


# Set communication parameters (baud rate, parity bits)
stty -F /dev/alice ispeed 115200 ospeed 11520 -ignpar cs8

# Open Alice rebooter in another TTY
exec 3<> /dev/alice

# Wait a bit because arduino needs time to setup
sleep 2

# Send control characters
echo $char >&3

# Close Alice rebooter TTY
exec 3>&-
